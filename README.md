# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Summary: 
 
This is a front-end styling test involving a profile about page (one single page but responsive) based on the designs provided in “Design” folder. Please follow the instructions very closely and 	checkmark each	 number below  for the following aspects: 


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

Requirements & Instructions: 
  
1.	___ Tip: do not underestimate this test in terms of implementation and your submission deadline. Each aspect of the test and instructions are given to test certain specific skills required by our team. 
2.	___ The design for desktop and responsive mobile have been provided for each screen. Please follow it very closely and replicate them as closely as you can. 
3.	___ You have the option to use the PSD file (see "PSD File" folder)  or also JPG images of each page to replicate the designed pages (see "JPG Files" folder). In the “Resources” folder you will find the profile image. 
4.	___ Notice that separate designs for desktop and mobile is provided for each screen. 
5.	___ Tablet design is not provided, but it will be the same as desktop except less wide. 
6.	___ The desktop design for field editing popup and hover (for edit pen) elements are shown. The pop-up design for other fields (such as website, name, etc) are not provided in design (only the pop-up for the city as an example is shown) but you can use the same concept as shown for those fields in desktop version. Editing in mobile is shown in the design and is completely different as you can see. Design inconsistencies between desktop and mobile are intentional. 
7.	___ Content for the popups should be generated/saved dynamically. 	Don’t hardcode content on each pop-up for example and make it functional. Replace content live as it is edited/saved in both the view section of the page and the top header (for fields being shown). Show us your scripting skills always in all pages! We recommend to use ES6 for this. 
8.	___ The menu (about, settings, option1, etc) for mobile version should operate by swapping 	to the right or	 left as you can see in the mobile design. Again, it must be swappable and we should not 	see a scroll bar when testing it on both different types of browsers! (see design look) 
9.	___ The header for the profile page both in desktop and mobile must be a shared component for all pages/tabs so that if you make a change to the header once, it will change the header automatically for all other pages/tabs. Changing through the menu tabs should be dynamic, rendering each tab. 
10.	___ For icons (such as plus, star, pen, etc), use: http://ionicons.com/cheatsheet.html 
11.	___ All fields should follow the Google material design which has the title of the field in the text box by default; once the user input is provided, the title moves above the field and becomes a small overhead title and line becomes blue when focused; see for example: https://material.angularjs.org/latest/demo/input   ---	 however, please note that you should not use angular or other “js frameworks” for this test, only ES6 JS (which is the only JS used on our platform along with Jquery). Again, the link to material design is for example purpose only. 
12.	___ Must use BEM (http://getbem.com/introduction/) to organize CSS (we use this technology in our platform) 
13.	___ Must use SASS for precompiler (we use this technology in our platform) 
14.	___ Must use Gulp for SASS and Webpack for ES6  part (we use these technologies in our platform). Or just Webpack. We would like to particularly see your understanding of WebPack. 
15.	___ Test your pages in the following browsers and version for compatibility: Chrome (older and newest version), Edge (older and newest version), and Safari (older and newest version) to make sure the pages look the same as the design and there are no issues with responsive pages. We will view your results in these 3 browsers and two versions for each (older and newest) on both desktop and mobile sizes. If you don’t have any of these browsers and can’t download them or signup for a free trial at browserstack.com (which is what we use in our team). Browser compatibility development is a major frontend skill and points will be deducted if not compatible. 
16.	___ When you start coding this test, in the very beginning, setup Bitbucket repo and commit as you continue to code and make progress until you’re done & make the final commit. After you’re done and you share the link for review (only after completion), we will need to see your commit times/dates. 
17.	___ Please note that the UI design that is being provided to you is specifically designed for this test and is not the current or working platform’s design for confidentiality reasons. Please do not worry about the UI/UX issues and there is no need to make recommendations for UI. Also, note that design inconsistencies are intentional.  
18.	___ Attention to details and  following these instructions, AND testing is VERY important aspect of this test. 
19.	___ FINALLY, use best programming practices because although we have given you these instructions and minimum requirements, at the end of the day, when we review your code, we need to note your skills, standard or expected practices, and your weight with best approaches in frontend programming. Structure and MVC is very important and part of our scoring system; as a tip, try to avoid hardcoding html into JS (as possible) and make your JS more component based instead of creating one single file. 

Technologies to be used in this test (ONLY): 
 
●	For javascript, ONLY implement ES6+ javascript. Try to show your scripting skills. 
●	HTML5 
●	CSS3 and SASS 
●	Note: Do NOT use Bootstrap; Do NOT use any other javascript technologies other than ES6+ (make sure not to use Jquery, Angular, Vue, React, or any other JS frameworks/Lib). We are aware that it is easier to use a framework. This test intentionally forbids	 any use of libraries, templates, or frameworks; we want to	 see your raw skills and organization. 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

How to turn in the results after completion:  
 
1.	First, deposit all your code in a public 	repository on BitBucket and share it with us so that we can see your coding, organization, approach, standards, skills, etc. Please name the repository folder as your first two letters of first name then underscore followed by first two letters of your last name; so if your name was 
John Doe for example the name of your repo would be “JO_DO” …. Do not put the name “cyza” anywhere (no files and no urls should be named cyza). You should email the link to your public repository to the email: 
team@cyza.com  AND: 
2.	Second, provide a temporary server link to the live application so that we can see it working live and test it. 
Please email the link to the same: team@cyza.com 
--Link your pages so that we can click through them. Without the live server link, we will not  review your test results. Also, please do not use the name “cyza” in the url or naming of any file or folders. The live server link should run and be accessible at anytime (better to use a third-party host that is always live and accessible).  


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact